<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Setting</font></h4>
              <div class="row">
                <div class="col-md-12">
                  <h2><font color="#093C7D">Pengaturan Organisasi</font></h2>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#menu1">Perusahaan</a></li>
                    <li><a data-toggle="tab" href="#menu2" >Penjualan</a></li>
                    <li><a data-toggle="tab" href="#menu3" >Pembelian</a></li>
                    <li><a data-toggle="tab" href="#menu4" >Product</a></li>
                    <li><a data-toggle="tab" href="#menu5" >Pengaturan Pengguna</a></li>
                    <li><a data-toggle="tab" href="#menu6" >Tagihan</a></li>
                  </ul>

                      <!-- mulai isi konten -->
                  <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                    <br><br>
                      <div class="col-md-6">
                        <form class="form-horizontal">
                            <div class="form-group">
                              <label class="col-md-3" for="exampleInputFile">Logo</label>
                                <div class="col-md-3">
                                   <input type="file" id="exampleInputFile">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Tampilkan Logo di Laporan</label>
                                <div class="col-sm-8">
                                  <input type="checkbox">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Nama Perusahaan</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Alamat</label>
                                <div class="col-sm-8">
                                  <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Alamat Pengiriman</label>
                                <div class="col-sm-8">
                                  <textarea class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Telepon</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Fax</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">NPWP</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Website</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Email</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Tag Transaksi</label>
                                <div class="col-sm-1">
                                  <input type="checkbox">
                                </div>
                                <div class="col-sm-2">
                                  <font color="#449D44"><a class="ikon" href=""><span class="glyphicon glyphicon-share-alt"></span></a></font>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Mata Uang</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3">Format Mata Uang</label>
                                <div class="col-sm-6">
                                  <select name="" class="form-control">
                                    <option value="1,2">One With Decimals</option>
                                    <option value="1,0">Once</option>
                                    <option value="1000,1">in Thousand</option>
                                    <option value="1000000,1">in Million</option>
                                  </select>
                                </div>
                            </div>
                        </form>
                      </div>
                      <div class="col-md-6">
                      <h3>DETAIL AKUN BANK</h3>
                        <form class="form-horizontal">
                            <div class="form-group">
                              <label class="col-md-1">Nama Bank</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-1">Cabang Bank</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-1">Alamat Bank</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-1">Nomor Akun Bank</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-1">Atas Nama</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-1">Swift Code</label>
                                <div class="col-md-5">
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>  
                    <div id="menu2" class="tab-pane fade">
                    <br><br>
                      <div class="col-md-6">
                        <form class="form-horizontal">
                          <div class="form-group">
                            <label class="col-md-3">Syarat Pembayaran Penjualan Utama</label>
                              <div class="col-sm-6">
                                <select name="" class="form-control">
                                    <option value="1">Net 30</option>
                                    <option value="2">Cash On Delivery</option>
                                    <option value="3">Net 15</option>
                                    <option value="4">Net 60</option>
                                    <option value="5">Custom</option>
                                </select>
                              </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Pengiriman</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Diskon</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Diskon Per Baris</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Uang Muka</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Tampilkan % Keuntungan di Fatkur Penjualan</label>
                              <div class="col-sm-8">
                                <input type="checkbox"> <span class="glyphicon glyphicon-question-sign"></span>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Pesan Penjualan Standar</label>
                              <div class="col-sm-8">
                                <textarea class="form-control" rows="3"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Pesan Surat Jalan Standar</label>
                              <div class="col-sm-8">
                                <textarea class="form-control" rows="3"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3"></label>
                              <div class="col-sm-8">
                                <a class="btn btn-danger" href="<?= base_url('') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</a>
                                <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Ubah</button>
                              </div>
                          </div>
                        </form>
                      </div> 
                    </div>
                    <div id="menu3" class="tab-pane fade">
                    <br><br>
                      <div class="col-md-6">
                        <form class="form-horizontal">
                          <div class="form-group">
                            <label class="col-md-3">Syarat Pembayaran Pembelian Utama</label>
                              <div class="col-sm-6">
                                <select name="" class="form-control">
                                    <option value="1">Net 30</option>
                                    <option value="2">Cash On Delivery</option>
                                    <option value="3">Net 15</option>
                                    <option value="4">Net 60</option>
                                    <option value="5">Custom</option>
                                </select>
                              </div> 
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Pengiriman</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Diskon</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Diskon Per Baris</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Uang Muka</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Pesan Pesanan Pembelian Standar</label>
                              <div class="col-sm-8">
                                <textarea class="form-control" rows="3"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3"></label>
                              <div class="col-sm-8">
                                <a class="btn btn-danger" href="<?= base_url('') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</a>
                                <button class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Ubah</button>
                              </div>
                          </div>
                        </form>
                      </div> 
                    </div>
                    <div id="menu4" class="tab-pane fade">
                     <br><br>
                      <div class="col-md-6">
                        <form class="form-horizontal">
                          <div class="form-group">
                            <label class="col-md-3">Tampilkan gambar produk di tabel daftar produk</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Monitor Stok Persediaan Barang</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                                <font color="#449D44"> <a class="ikon" href=""><span class="glyphicon glyphicon-share-alt"></span></a></font>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3">Kategori Produk</label>
                              <div class="col-sm-8">
                                <input type="checkbox">
                                <font color="#449D44"> <a class="ikon" href=""><span class="glyphicon glyphicon-share-alt"></span></a></font>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3"></label>
                              <div class="col-sm-8">
                                <a class="btn btn-danger" href="<?= base_url('') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</a>
                                <button class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Ubah</button>
                              </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div id="menu5" class="tab-pane fade">
                      <div class="box-tab">
                        <div class="box-tab-header with-border4">
                          <h3 div class="col-md-10">DAFTAR PENGGUNA</h3>
                          <div class="col-md-2">
                            <a class="btn btn-info" href="#"><i class='fa fa-plus'></i> <span> Buat Biaya Baru</span></a>
                          </div>
                        </div>
                        <div class="box-body">
                          <table class="table">
                            <tr>
                              <th class="th">Nama</th>
                              <th class="th">Email</th>
                              <th class="th">Roles</th>
                              <th class="th">Status</th>
                              <th class="th">Tindakan</th>
                            </tr>
                            <tr>
                              <td class="td">Eka Novita Christin</td>
                              <td class="td">ekavita.30@gmail.com</td>
                              <td class="td">Owner</td>
                              <td class="td">Terdaftar</td>
                              <td class="td"><a class="kolom" href="#">Lihat</a></td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div id="menu6" class="tab-pane fade">
                      <div class="col-md-offset-9 col-md-3">
                        <h3>Eka's Group</h3>
                        <h5><font color="red">Active</font></h5>
                      </div>
                      <div class="col-md-4">
                        <div class="kotak">
                          <h4>PAKET BERLANGGANAN</h4>
                          <hr>
                            <p><font align="justify">Paket &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trial Starter</font></p>
                            <p><font align="justify">Pengguna &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 dari -</font></p>
                            <p><font align="justify">Sisa Hari  :</font></p>
                            <p><font align="justify">Tanggal selesai :</font></p>    
                        </div>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>