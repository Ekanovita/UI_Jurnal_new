<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
              <h4><font color="gray">Laporan</font></h4>
              <div class="row">
                <div class="col-md-6">
                  <h2><font color="#093C7D">Neraca</font></h2>
                </div>
                <div class="col-md-6">
                  <a class="btn btn-success" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <span> Tampilkan PDF</span></a>
                  <a class="btn btn-success" href="#"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span> Tampilkan Excel</span></a>
                  <a class="btn btn-success" href="#"><i class="fa fa-table" aria-hidden="true"></i> <span> Tampilkan CSV</span></a>
                </div>
              </div>
              <hr>
                <div class="row">
                  <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                      <form class="form-horizontal">
                        <div class="row">
                          <div class="form-group">
                            <label class="col-md-2">per :</label>
                              <div class="col-md-3">
                                <select name="" class="form-control">
                                  <option value="1">Tanggal</option>
                                  <option value="2">Hari ini</option>
                                  <option value="3">Minggu ini</option>
                                  <option value="4">Bulan ini</option>
                                  <option value="5">Minggu lalu</option>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <div class='input-group date' id='datepicker'>
                                  <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                              </div>
                          </div>
                        </div>
                      </form>
                  </div>
                  <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                    <form class="form-horizontal">
                      <div class="row">
                        <div class="form-group">
                          <label class="col-md-2">Bandingkan Periode :</label>
                            <div class="col-md-3">
                              <select name="" class="form-control">
                                <option value="1">none</option>
                                <option value="2">Mingguan</option>
                                <option value="3">Bulanan</option>
                                <option value="4">Triwulan</option>
                                <option value="5">Tahunan</option>
                              </select>
                            </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                    <div class="row">
                      <a class="btn btn-info" href="#"><i class="fa fa-filter" aria-hidden="true"></i> <span> Filter</span></a>
                    </div>
                  </div>
                </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-offset-5 col-md-2 col-md-offset-5">
                    <center><h3>EKA'S GROUP</h3></center>
                    <p><center><h3>Neraca</h3></center></p>
                    <P><center>per Tanggal (d/m/y)</center></P>
                    <p><center>(dalam IDR)</center></p>
                  </div>
                  <div class="col-md-12">
                    <table class="table">
                      <tr>
                        <th class="th">Tanggal</th>
                        <th class="th3">d/m/y</th>
                      </tr>
                      <tr>
                        <td class="td3">aktiva</td>
                        <td class="td2"></td>
                      </tr>
                      <tr>
                        <th class="th4">Total Aktiva</th>
                        <th class="th4">0,00</th>
                      </tr>
                      <tr>
                        <td class="td3">Kewajiban Dan Modal</td>
                        <td class="td3"></td>
                      </tr>
                      <tr>
                        <th class="th4">Total Kewajiban Dan Modal</th>
                        <th class="th4"></th>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

<?php endsection() ?>
<?php getview('layouts/home') ?>