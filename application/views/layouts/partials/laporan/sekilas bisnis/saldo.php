<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
              <h4><font color="gray">Laporan</font></h4>
              <div class="row">
                <div class="col-md-6">
                  <h2><font color="#093C7D">Jurnal</font></h2>
                </div>
                <div class="col-md-6">
                  <a class="btn btn-success" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <span> Tampilkan PDF</span></a>
                  <a class="btn btn-success" href="#"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span> Tampilkan Excel</span></a>
                  <a class="btn btn-success" href="#"><i class="fa fa-table" aria-hidden="true"></i> <span> Tampilkan CSV</span></a>
                </div>
              </div>
              <hr>
                <div class="row">
                  <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                      <form class="form-horizontal">
                        <div class="row">
                          <div class="form-group">
                            <label class="col-md-1">Filter sesuai periode :</label>
                              <div class="col-md-3">
                                <select name="" class="form-control">
                                  <option value="1">Tanggal</option>
                                  <option value="2">Hari ini</option>
                                  <option value="3">Minggu ini</option>
                                  <option value="4">Bulan ini</option>
                                  <option value="5">Minggu lalu</option>
                                </select>
                              </div>
                            <label class="col-md-1">Tanggal mulai:</label>
                              <div class="col-md-3">
                                <div class='input-group date' id='datepicker'>
                                  <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                              </div>
                            <label class="col-md-1">Tanggal Selesai:</label>
                              <div class="col-md-3">
                                <div class='input-group date' id='datepicker'>
                                  <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                              </div>
                          </div>
                        </div>
                      </form>
                  </div>
                  <div class="col-md-offset-5 col-md-7">
                    <div class="row">
                      <a class="btn btn-info" href="#"><i class="fa fa-filter" aria-hidden="true"></i> <span> Filter</span></a>
                    </div>
                  </div>
                </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-offset-4 col-md-4 col-md-offset-4">
                    <center><h3>EKA'S GROUP</h3></center>
                    <p><center><h3>Neraca Saldo</h3></center></p>
                    <p><center>d/m/y</center></p>
                    <p><center>(dalam IDR)</center></P>
                  </div>
                  <div class="col-md-12">
                    <table class="table table-striped table-bordered" border="1">    
                      <thead> 
                        <tr class="tr1 danger">              
                          <th rowspan="2"><center>Daftar Akun</center></th>
                          <th colspan="2"><center>Saldo Awal</center></th>
                          <th colspan="2"><center>Pergerakan</center></th>
                          <th colspan="2"><center>Saldo Akhir</center></th>
                        </tr>
                        <tr class="tr1 danger">
                          <th>Debit</th>
                          <th>Kredit</th>
                          <th>Debit</th>
                          <th>Kredit</th>
                          <th>Debit</th>
                          <th>Kredit</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="tr1">
                          <td>Aset</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="tr1">
                          <td>Kewajiban</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="tr1">
                          <td>Ekuitas</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="tr1">
                          <td>Pendapatan</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="tr1">
                          <td>Beban</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="tr1">
                          <td>Total</td>
                          <td>0,00</td>
                          <td>0,00</td>
                          <td>0,00</td>
                          <td>0,00</td>
                          <td>0,00</td>
                          <td>0,00</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

<?php endsection() ?>
<?php getview('layouts/home') ?>