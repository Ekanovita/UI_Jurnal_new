<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Laporan</font></h4>
              <div class="col=md-12">
                <h2><font color="#093C7D">Laporan Akutansi</font></h2>
              </div>
            </header>
          </div>
        </section>
        <section>
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a class="color" href="#tab1" data-toggle="tab">Sekilas Bisnis</a></li>
                  <li><a class="color" href="#tab2" data-toggle="tab">Penjualan</a></li>
                  <li><a class="color" href="#tab3" data-toggle="tab">Pembelian</a></li>
                  <li><a class="color" href="#tab4" data-toggle="tab">Produk</a></li>
                  <li><a class="color" href="#tab5" data-toggle="tab">Aset</a></li>
                </ul>
                <div class="tab-content">
                  <div id="tab1" class="tab-pane fade in active">
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/neraca') ?>">NERACA</a></h3>
                        <p>Menampilan apa yang anda miliki (aset), apa yang anda hutang (liabilitas), dan apa yang anda sudah investasikan pada perusahaan anda (ekuitas).</p>
                      <a href="<?= base_url('admin/neraca') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/labarugi') ?>">LAPORAN LABA RUGI</a></font></h3>
                        <p>Menampilkan setiap tipe transaksi dan jumlah total untuk pendapatan dan pengeluaran anda.</p>
                      <a href="<?= base_url('admin/labarugi') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/aruskas') ?>">ARUS KAS</a></font></h3>
                        <p>Laporan ini mengukur kas yang telah dihasilkan atau digunakan oleh suatu perusahaan dan menunjukkan detail pergerakannya dalam suatu periode.</p>
                      <a href="<?= base_url('admin/aruskas') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/bukubesar') ?>">BUKU BESAR</a></font></h3>
                        <p>Laporan ini menampilkan semua transaksi yang telah dilakukan untuk suatu periode. Laporan ini bermanfaat jika Anda memerlukandaftar kronologis untuk semua transaksi yang telah dilakukan oleh perusahaan Anda.</p>
                      <a href="<?= base_url('admin/bukubesar') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/rekonsiliasi') ?>">RINGKASAN REKONSILIASI BANK</a></font></h3>
                        <p>Menampilkan seluruh daftar rekonsiliasi bank yg pernah dilakukan untuk semua akun kas & bank sebelumnya.</p>
                      <a href="<?= base_url('admin/rekonsiliasi') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/jurnal') ?>">JURNAL</a></font></h3>
                        <p>daftar semua jurnal per transaksi yang terjadi dalam periode waktu. Hal ini berguna untuk melacak di mana transaksi Anda masuk ke masing-masing rekening.</p>
                      <a href="<?= base_url('admin/jurnal') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/saldo') ?>">NERACA SALDO</a></font></h3>
                        <p>Menampilkan saldo dari setiap akun, termasuk saldo awal, pergerakan, dan saldo akhir dari periode yang ditentukan.</p>
                      <a href="<?= base_url('admin/saldo') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                  </div>
                  <div id="tab2" class="tab-pane fade">
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/faktur') ?>">DAFTAR PENJUALAN</a></font></h3>
                        <p>Menunjukkan daftar kronologis dari semua faktur dan pembayaran untuk rentang tanggal yang dipilih.</p>
                      <a href="<?= base_url('admin/faktur') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/piutang') ?>">LAPORAN PIUTANG PELANGGAN</a></font></h3>
                        <p>Menampilkan tagihan yang belum dibayar untuk setiap pelanggan, termasuk nomor & tanggal faktur, tanggal jatuh tempo, jumlah nilai, dan sisa tagihan yang terhutang pada Anda.</p>
                      <a href="<?= base_url('admin/piutang') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/pengiriman') ?>">LAPORAN PENGIRIMAN PENJUALAN</a></font></h3>
                        <p>Menampilkan semua produk yang dicatat terkirim untuk transaksi penjualan dalam suatu periode, dikelompok per pelanggan.</p>
                      <a href="<?= base_url('admin/pengiriman') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/penjualanplgn') ?>">PENJUALAN PER PELANGGAN</a></font></h3>
                        <p>Menampilkan setiap transaksi penjualan untuk setiap pelanggan, termasuk tanggal, tipe, jumlah dan total.</p>
                      <a href="<?= base_url('admin/penjualanplgn') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/usiapiutang') ?>">DAFTAR UMUR PIUTANG</a></font></h3>
                        <p>Laporan ini memberikan ringkasan piutang Anda, yang menunjukkan setiap pelanggan karena Anda secara bulanan, serta jumlah total dari waktu ke waktu. Hal ini praktis untuk membantu melacak piutang Anda.</p>
                      <a href="<?= base_url('admin/usiapiutang') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/penjualanproduct') ?>">PENJUALAN PER PRODUCT</a></font></h3>
                        <p>Menampilkan daftar kuantitas penjualan per produk, termasuk jumlah retur, net penjualan, dan harga penjualan rata-rata.</p>
                      <a href="<?= base_url('admin/penjualanproduct') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                  </div>
                  <div id="tab3" class="tab-pane fade">
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/daftarbeli') ?>">DAFTAR PEMBELIAN</a></font></h3>
                        <p>Menampilkan daftar kronologis untuk semua pembelian dan pembayaran Anda untuk rentang tanggal yang dipilih.</p>
                      <a href="<?= base_url('admin/daftarbeli') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/hutangsupplier') ?>">LAPORAN HUTANG SUPPLIER</a></font></h3>
                        <p>Menampilkan jumlah nilai yang Anda hutang pada setiap Supplier.</p>
                      <a href="<?= base_url('admin/hutangsupplier') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/pengeluaran') ?>">RINCIAN PENGELUARAN</a></font></h3>
                        <p>Laporan ini merincikan pengeluaran-2, dan dikelompokan dalam kategori masing2 dalam jangka waktu yg Anda tentukan.</p>
                      <a href="<?= base_url('admin/pengeluaran') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/pengirimanbeli') ?>">LAPORAN PENGIRIMAN PEMBELIAN</a></font></h3>
                        <p>Menampilkan semua produk yang dicatat terkirim untuk transaksi pembelian dalam suatu periode, dikelompok per supplier.</p>
                      <a href="<?= base_url('admin/pengirimanbeli') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                    <div class="col-md-6">
                      <h3><a class="style" href="<?= base_url('admin/pembelianper') ?>">PEMBELIAN PER SUPPLIER</a></font></h3>
                        <p>Menampilkan setiap pembelian dan jumlah untuk setiap Supplier.</p>
                      <a href="<?= base_url('admin/pembelianper') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/dftrpengeluaran') ?>">DAFTAR PENGELUARAN</a></font></h3>
                        <p>Daftar seluruh pengeluaran dengan keterangannya untuk kurung waktu yg ditentukan.</p>
                      <a href="<?= base_url('admin/dftrpengeluaran') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/piutangbeli') ?>">DAFTAR UMUR PIUTANG</a></font></h3>
                        <p>Laporan ini memberikan ringkasan hutang Anda, menunjukkan setiap vendor Anda secara bulanan, serta jumlah total dari waktu ke waktu. Hal ini praktis untuk membantu melacak hutang Anda.</p>
                      <a href="<?= base_url('admin/piutangbeli') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                      <h3><a class="style" href="<?= base_url('admin/pembelianproduct') ?>">PEMBELIAN PER PRODUCT</a></font></h3>
                        <p>Menampilkan daftar kuantitas pembelian per produk, termasuk jumlah retur, net pembelian, dan harga pembelian rata-rata.</p>
                      <a href="<?= base_url('admin/pembelianproduct') ?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a>
                      <br><br>
                    </div>
                  </div>
                  <div id="tab4" class="tab-pane fade">
                    <div class="col-md-6">
                      <h3><a class="style" href="#">RINGKASAN PERSEDIAAN BARANG</a></font></h3>
                        <p>Menampilkan daftar kuantitas dan nilai seluruh barang persediaan per tanggal yg ditentukan.</p>
                      <a href="#" class="btn btn-primary disabled"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a><font color="#449D44"> <span class=" glyphicon glyphicon-share-alt"></span></font>
                      <br><br>
                      <h3><a class="style" href="#">NILAI PERSEDIAAN BARANG</a></font></h3>
                        <p>Rangkuman informasi penting seperti sisa stok yg tersedia, nilai, dan biaya rata-rata, untuk setiap barang persediaan.</p>
                      <a href="#" class="btn btn-primary disabled"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a><font color="#449D44"> <span class=" glyphicon glyphicon-share-alt"></span></font>
                      <br><br>
                      <h3><a class="style" href="#">RINCIAN PERSEDIAAN BARANG</a></font></h3>
                        <p>Menampilkan daftar transaksi yg terkait dengan setiap Barang dan Jasa, dan menjelaskan bagaimana transaksi tersebut mempengaruhi jumlah stok barang, nilai, dan harga biaya nya.</p>
                      <a href="#" class="btn btn-primary disabled"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a><font color="#449D44"> <span class=" glyphicon glyphicon-share-alt"></span></font>
                      <br><br>
                    </div>
                  </div>
                  <div id="tab5" class="tab-pane fade">
                    <div class="col-md-6">
                      <h3><a class="style" href="#">RINGKASAN ASET TETAP</a></font></h3>
                        <p>Menampilkan daftar aset tetap yang tercatat, dengan harga awal, akumulasi penyusutan, dan nilai bukunya.</p>
                      <a href="#" class="btn btn-primary disabled"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a><font color="#449D44"> <span class=" glyphicon glyphicon-share-alt"></span></font>
                      <br><br>
                      <h3><a class="style" href="#">DETAIL ASET TETAP</a></font></h3>
                        <p>Menampilkan daftar transaksi yg terkait dengan setiap aset, dan menjelaskan bagaimana transaksi tersebut mempengaruhi nilai bukunya.</p>
                      <a href="#" class="btn btn-primary disabled"><i class="fa fa-eye" aria-hidden="true"></i><span>Lihat Laporan</span></a><font color="#449D44"> <span class=" glyphicon glyphicon-share-alt"></span></font>
                      <br><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>
