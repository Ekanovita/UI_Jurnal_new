<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
              <h1><font color="#093C7D">Eka Novita Christin</font></h1>
              <div class="row">
                <div class="col-md-12">
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
              <div class="row">
                <div class="col-md-12">
                  <div class="box4">
                    <div class="box-header4 with-border4">
                      <h5 class="box-title col-md-8">Daftar Perusahaan</h5>
                      <div class="col-md-4">
                        <a class="btn btn-info" href="<?= base_url('admin/profil') ?>">Profil Akun</a>
                        <a class="btn btn-info" href="#daftar" data-toggle="modal">Buat Perusahaan Baru</a>
                      </div>
                    </div>
                    <div class="box-body">
                      <table class="table">
                        <thead>
                          <tr>
                            <th class="th">Nama Perusahaan</th>
                            <th class="th">Peran</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="td"><a class="kolom" href="<?= base_url('') ?>">EKA'S GROUP</a></td>
                            <td class="td">Ultimate</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
        </section>
        <div id="daftar" class="modal fade">
          <div class="modal-dialog">
            <h1><center><font color="white"><b>ACCOUNTING</b></font></center></h1>
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Informasi Perusahaan</font></h2>
              </div>
              <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label class="control-label">Nama Perusahaan:</label>
                            <input type="text" class="form-control" placeholder="Nama Perusahaan">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Industri Perusahaan :</label>
                            <input type="text" class="form-control" placeholder="Industri Perusahaan">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Bahasa Pilihan :</label>
                            <select class="form-control" name="">
                              <option value="English">English</option>
                              <option value="Bahasa Indonesia">Bahasa Indonesia</option>
                            </select>
                      </div>
                    </form>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-info col-md-offset-1 col-md-10 col-md-offset-1">Buat Perusahaan</button>
              </div>
            </div>
          </div>
        </div>  
<?php endsection() ?>
<?php getview('layouts/home') ?>