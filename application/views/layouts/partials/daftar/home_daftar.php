<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Setting</font></h4>
              <div class="row">
                <div class="col-md-12">
                  <h2><font color="#093C7D">Daftar Lainnya</font></h2>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-6">
                    <h3><a class="style" href="<?= base_url('admin/daftar_transaksi') ?>">DAFTAR TRANSAKSI</a></h3>
                      <p>Daftar dari semua transaksi</p>
                      <br><br>
                    <h3><a class="style" href="#">DAFTAR LAMPIRAN</a></h3>
                      <p>Daftar semua lampiran pada transaksi</p>
                      <br><br>
                    <h3><a class="style" href="#">DAFTAR REKONSILIASI BANK</a></h3>
                      <p>Menampilkan seluruh daftar rekonsiliasi bank yg pernah dilakukan untuk semua akun kas & bank sebelumnya.</p>
                      <br><br>
                    <h3><a class="style" href="#">PAJAK</a></h3>
                      <p>Menampilkan tipe-tipe pajak yang Anda pakai untuk penjualan kepada pelanggan atau pembelian dari supplier.</p>
                      <br><br>
                    <h3><a class="style" href="#">SYARAT PEMBAYARAN</a></h3>
                      <p>Menampilkan daftar syarat pembayaran yang menentukan tanggal jatuh tempo untuk pelanggan atau dari supplier. Dari sini Anda dapat    membuat syarat pembayaran baru, dan mengubah atau menghapusnya.</p>
                      <br><br>
                  </div>
                  <div class="col-md-6">
                    <h3><a class="style" href="#">DAFTAR JADWAL BERULANG</a></h3> 
                      <p>Daftar pengaturan jadwal untuk transaksi berulang</p>
                      <br><br>
                    <h3><a class="style" href="#">DAFTAR PENUTUPAN BUKU</a></h3> 
                      <p>Menampilkan daftar tutup buku per periode, termasuk dengan laporan-laporan per periode</p>
                      <br><br>
                    <h3><a class="style" href="#">CARA PEMBAYARAN</a></h3> 
                      <p>Menampilkan cara pembayaran yang Anda pakai atau dipakai oleh Supplier Anda.</p>
                      <br><br>
                    <h3><a class="style" href="#">SATUAN PRODUCT</a></h3> 
                      <p>Menampilkan tipe-tipe satuan produk yang Anda pakai untuk menentukan satuan yang digunakan untuk mencatat product.</p>
                      <br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>