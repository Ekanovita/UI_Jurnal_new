<?php section('content') ?>
                  <div id='main-content'>
                    <header class='page-heading'>
                      <font color="#C78A87">Masa percobaan berakhir dalam  14  hari</font>
                        <div class='row'>
                          <div class='col-md-8 col-sm-12 col-xs-12'>
                              <h2>
                                Gambar Ringkasan Bisnis
                              </h2>
                            <div class='dropdown' style='float:left; margin-top:9px; margin-left:11px;'>
                              <button aria-expanded='false' aria-haspopup='true' data-toggle='dropdown' id='dLabel' type='button'>
                                month
                                  <span class='fa fa-chevron-down' style='margin-left: 9px'></span>
                              </button>
                              <ul aria-labelledby='dLabel' class='dropdown-menu size-menu-open' style='padding: 5px 15px;'>
                                <li>
                                  <form class='filter_class' id='dashboard_date_filter'>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='11/04/2016' id='inlineRadio1' name='inlineRadioOptions' start-data-value='11/04/2016' type='radio' value='day'>
                                        <label>
                                          hari ini
                                        </label>
                                    </div>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='17/04/2016' id='inlineRadio2' name='inlineRadioOptions' start-data-value='11/04/2016' type='radio' value='week'>
                                        <label>
                                          minggu ini
                                        </label>
                                    </div>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='30/04/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='month'>
                                        <label>
                                          bulan ini
                                        </label>
                                    </div>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='30/06/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='quarter'>
                                        <label>
                                          kuartal ini
                                        </label>
                                    </div>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='31/12/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/01/2016' type='radio' value='year'>
                                        <label>
                                          tahun ini
                                        </label>
                                    </div>
                                    <div class='checkboxgroup'>
                                      <input end-data-value='30/04/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='custom'>
                                        <label>
                                          sesuaikan
                                        </label>
                                    </div>
                                  </form>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class='col-md-4 col-sm-12 col-xs-12'>
                            <a class="botton" href="<?= base_url('admin/setting') ?>">Upload Logo Perusahaan</a>
                          </div>
                        </div>
                    </header>
                  </div>
                </section>
                <section class="content">
                  <div class="row">
                    <div class="col-md-3">
                      <h3>Penjualan (tahun ini)</h3>
                        <div class="box"><a class="a" href="<?= base_url('admin/penjualan') ?>">
                          <div class="box-header with-border">
                            <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small><small class="label pull-right bg-yellow">0</small>
                          </div>
                          <div class="box-body">
                            <h3>Rp 0,00</h3>
                          </div><!-- /.box-body --><!-- /.box-footer-->
                        </div></a>
                    </div>
                    <div class="col-md-3">
                      <h3>Pembelian (tahun ini)</h3>
                        <div class="box"><a class="a" href="<?= base_url('admin/pembelian') ?>">
                          <div class="box-header with-border">
                            <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small><small class="label pull-right bg-yellow">0</small>
                          </div>
                          <div class="box-body">
                            <h3>Rp 0,00</h3>
                          </div><!-- /.box-body --><!-- /.box-footer-->
                        </div></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                        <div class="box1"><a class="a" href="<?= base_url('admin/penjualan') ?>">
                          <div class="box-header1 with-border1">
                            <h4 class="box-title">Jatuh Tempo&nbsp;(dalam IDR)<small class="label pull-right bg-red">0</small></h4>
                          </div>
                          <div class="box-body">
                            <h3>Rp 0,00</h3>
                          </div><!-- /.box-body --><!-- /.box-footer-->
                        </div></a>
                    </div>
                    <div class="col-md-3">
                      <div class="box1"><a class="a" href="<?= base_url('admin/pembelian') ?>">
                        <div class="box-header1 with-border1">
                          <h4 class="box-title">Jatuh Tempo&nbsp;(dalam IDR)<small class="label pull-right bg-red">0</small></h4>
                        </div>
                        <div class="box-body">
                          <h3>Rp 0,00</h3>
                        </div><!-- /.box-body --><!-- /.box-footer-->
                      </div></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                        <div class="box2"><a class="a" href="<?= base_url('admin/penjualan') ?>">
                          <div class="box-header2 with-border2">
                            <h4 class="box-title">Pelunasan Diterima&nbsp;(dalam IDR)<small class="label pull-right bg-green">0</small></h4>
                          </div>
                          <div class="box-body">
                            <h3>Rp 0,00</h3>
                          </div><!-- /.box-body --><!-- /.box-footer-->
                        </div></a>
                    </div>
                    <div class="col-md-3">
                      <div class="box2"><a class="a" href="<?= base_url('admin/pembelian') ?>">
                        <div class="box-header2 with-border2">
                          <h4 class="box-title">Pelunasan Dibayar&nbsp;(dalam IDR)<small class="label pull-right bg-green">0</small></h4>
                        </div>
                        <div class="box-body">
                          <h3>Rp 0,00</h3>
                        </div><!-- /.box-body --><!-- /.box-footer-->
                      </div></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Keuntungan Usaha (bulan ini)</h3>
                        <div class="box3">
                          <div class="box-header3 with-border2">
                            <h4 class="box-title">Laba Bersih (dalam IDR)</h4>
                          </div>
                          <div class="box-body">
                            <h5>Total</h5>
                              <div class="col-md-4">
                                <table class="table">
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Nov'15</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Dec'15</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Jan'16</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Feb'16</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Mar'16</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                  <tr>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Apr'16</a></td>
                                    <td><a class="kolom" href="<?= base_url('admin/labarugi') ?>">Rp 0,00</a></td>
                                  </tr>
                                </table>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>