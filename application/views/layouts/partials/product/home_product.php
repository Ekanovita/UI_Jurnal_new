<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Product</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Barang & Jasa</font></h2>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-info" href="<?= base_url('admin/buatproduct') ?>"><i class='fa fa-plus'></i> <span> Buat Product Baru</span></a>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box4">
                <div class="box-header4 with-border4">
                  <h3 class="col-md-6">DAFTAR PRODUCT</h3>
                    <div class="col-md-6">
                      <form class="form-inline">
                        <div class="form-group">
                          <a class="btn btn-default" href="#"><i class="fa fa-download" aria-hidden="true"></i> Import</a>
                            <a class="btn btn-default" href="#eksport" data-toggle="modal"><i class="fa fa-upload" aria-hidden="true"></i> Eksport</a>
                              <div class="input-group">
                                <input type="text" class="form-control" placeholder="Pencarian...">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                  </span>
                              </div><!-- /input-group -->
                        </div>
                      </form>
                    </div>
                  <div class="col-md-12">
                    <input type="checkbox"><span> Tampilkan Product Arsip</span>
                  </div>
                  <br>
                </div>
                <div class="box-body">
                  <table class="table">
                    <tr>
                      <th class="th1">Kode Product</th>
                      <th class="th1">Nama</th>
                      <th class="th1">Harga Beli<font color="black"> (dalam IDR)</font></th>
                      <th class="th1">Harga Jual<font color="black"> (dalam IDR)</font></th>
                    </tr>
                    <tr>
                      <td class="td"></td>
                      <td class="td">Penjualan</td>
                      <td class="td">0,00</td>
                      <td class="td">0,00</td>
                    </tr>
                  </table>
                </div>
                <div class="box-footer">
                    <h5>Menampilkan 1..1 dari 1 Baris</h5>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div id="eksport" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Pengaturan Eksport</font></h2>
              </div>
              <div class="modal-body">
                <h4>Masukkan :</h4>
                <hr>
                  <div class="col-md-offset-1">
                    <input type="checkbox">
                      <span>Harga Beli</span>
                  </div>
                  <div class="col-md-offset-1">
                    <input type="checkbox">
                      <span>Harga Jual</span>
                  </div>
                  <div class="col-md-offset-1">
                    <input type="checkbox">
                      <span>Kuantitas Sekarang</span>
                  </div>
                <hr>
                  <div class="col-md-offset-1">
                    <input type="checkbox">
                      <span>Mengecualikan Product Diluar Persediaan</span>
                  </div>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/product') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Eksport</button>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>