<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Product</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Buat Product Baru</font></h2>
                </div>
              </div>
              <hr><br>
              <div class="row">
                <div class="col-md-offset-1 col-md-11">
                  <h4>Informasi Detail</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label" for="exampleInputFile">Gambar</label>
                        <input type="file" id="exampleInputFile">
                    </div>
                  </form>
                </div>
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Nama</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Deskripsi</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                  </form>
                </div>
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Kode Product</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Satuan</label>
                        <select name="" class="form-control">
                          <option value="">Buah</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <h5><a class="kolom" href="#satuan" data-toggle="modal">+ Tambah Satuan</a></h5>
                    </div>
                  </form>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <input type="checkbox">
                    <span>Saya Beli Product Ini</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <form>
                    <div class="form-group">
                      <div class="col-md-4">
                        <label class="control-label">Harga Beli Satuan</label>
                          <input type="text" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <label class="control-label">Akun Pembelian</label>
                          <select class="form-control" name="">
                            <option value="">(5-5000)- Harga Pokok Penjualan (COGS) (Harga Pokok Penjualan</option>
                            <option value="">(5-5100)- Diskon Pembelian (Harga Pokok Penjualan</option>
                            <option value="">(5-5200)-Retur Pembelian (Harga Pokok Penjualan</option>
                          </select>
                      </div>
                      <div class="col-md-4">
                        <input type="checkbox">
                          <span>Kena Pajak Pembelian ?</span>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <input type="checkbox">
                    <span>Saya Beli Product Ini</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <form>
                    <div class="form-group">
                      <div class="col-md-4">
                        <label class="control-label">Harga Beli Satuan</label>
                          <input type="text" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <label class="control-label">Akun Pembelian</label>
                          <select class="form-control" name="">
                            <option value="">(5-5000)- Harga Pokok Penjualan (COGS) (Harga Pokok Penjualan</option>
                            <option value="">(5-5100)- Diskon Pembelian (Harga Pokok Penjualan</option>
                            <option value="">(5-5200)-Retur Pembelian (Harga Pokok Penjualan</option>
                          </select>
                      </div>
                      <div class="col-md-4">
                        <input type="checkbox">
                          <span>Kena Pajak Pembelian ?</span>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <br><br>
              <div class="row">
                  <div class="col-md-offset-7 col-md-5">
                    <div class="col-md-1">
                      <a class="btn btn-danger" href="<?= base_url('admin/product') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                      <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Product Baru</button>
                    </div>
                  </div>
              </div>
            </header>
          </div>
        </section>
        <div id="satuan" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Syarat Pembayaran Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatproduct') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>