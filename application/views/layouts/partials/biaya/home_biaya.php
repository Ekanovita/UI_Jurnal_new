<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Biaya</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Pengeluaran</font></h2>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-info" href="<?= base_url('admin/buatbiaya') ?>"><i class='fa fa-plus'></i> <span> Buat Biaya Baru</span></a>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-6">
              <div class="box3"><a class="a" href="#">
                <div class="box-header3 with-border3">
                  <h5 class="box-title">Total Biaya Bulan Ini (dalam IDR)<small class="label pull-right bg-blue">0</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
            <div class="col-md-6">
              <div class="box3"><a class="a" href="#">
                <div class="box-header3 with-border3">
                  <h5 class="box-title">Biaya 30-Hari Terakhir (dalam IDR)<small class="label pull-right bg-blue">0</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
          </div>
         <div class="row">
            <div class="col-md-12">
              <div class="box5">
                <div class="box-header5 with-border5">
                  <h3 class="col-md-8">DAFTAR BIAYA</h3>
                    <div class="col-md-4">
                      <form class="form-inline">
                        <div class="form-group">
                          <a class="btn btn-default" href="#"><i class="fa fa-download" aria-hidden="true"></i>Import</a>
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Pencarian...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </span>
                            </div><!-- /input-group -->
                        </div>
                      </form>
                    </div>
                </div>
                <div class="box-body">
                  <table class="table">
                    <tr class="tr">
                      <th class="th1">Tanggal</th>
                      <th class="th1">Nomor</th>
                      <th class="th1">Kategori</th>
                      <th class="th1">Penerima</th>
                      <th class="th1">Total<font color="black"> (dalam IDR)</font></th>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>