<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Biaya</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Buat Biaya</font></h2>
                </div>
              </div>
              <hr><br>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <form class="form-horizontal">
                      <div class="row">
                        <div class="form-group">
                          <label class="col-md-1">Bayar Dari</label>
                            <div class="col-md-3">
                              <select name="" class="selectpicker" data-live-search="true">
                                <option></option>
                                  <option>(1-1000)- Kas (Kas & Bank)</option>
                                  <option>(1-1001)- Rekening Bank</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                              <h5><a class="kolom" href="#bayaran" data-toggle="modal"> + Tambah baru</a></h5>
                            </div>
                            <div class=" col-md-offset-2 col-md-4">
                              <h1>Total Kuitansi<font color="blue"> (Rupiah)</font></h1>
                            </div>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Penerima</label>
                        <select name="" class="form-control">
                          <option value=""></option>
                        </select>
                    </div>
                    <h5><a class="kolom" href="#terima"data-toggle="modal">+tambah Penerima</a></h5>
                    <div class="form-group">
                      <label class="control-label">Memo</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                  </form>
                </div>
                <div class="col-md-2">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Tgl Transaksi</label>
                        <div class='input-group date' id='datepicker'>
                          <input type='text' class="form-control" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                  </form>
                </div>
                <div class="col-md-2">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Cara Pembayaran</label>
                        <select name="" class="form-control">
                          <option value="">Cek & Giro</option>
                          <option value="">Kartu Kredit</option>
                          <option value="">Khas Tunai</option>
                          <option value="">Transfer Bank</option>
                        </select>
                    </div>
                    <h5><a class="kolom" href="#carabayar" data-toggle="modal">+ Tambah</a></h5>
                  </form>
                </div>
                <div class="col-md-2">
                  <form>
                    <div class="form-group">
                      <label class="control-label">No. Biaya</label>
                        <input type="text" class="form-control">
                    </div>
                  </form>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="th">Kategori Biaya</th>
                          <th class="th">Deskripsi</th>
                          <th class="th">Jumlah</th>
                          <th class="th">Hapus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-2">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                      <select name="" class="selectpicker" data-live-search="true">
                                        <option value="1">(6-6000)- Iklan & Promosi (Beban)</option>
                                        <option value="2">(6-6001)- Piutang Tak Tertagih (Beban)</option>
                                        <option value="">(6-6002)- Bank (Beban)</option>
                                        <option value="">(6-6003)- Kontribusi Sosial (Beban)</option>
                                        <option value="">(6-6004)- Biaya Tenaga Kerja</option>
                                      </select>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-2">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                      <select name="" class="selectpicker" data-live-search="true">
                                        <option value="1">(6-6000)- Iklan & Promosi (Beban)</option>
                                        <option value="2">(6-6001)- Piutang Tak Tertagih (Beban)</option>
                                        <option value="">(6-6002)- Bank (Beban)</option>
                                        <option value="">(6-6003)- Kontribusi Sosial (Beban)</option>
                                        <option value="">(6-6004)- Biaya Tenaga Kerja</option>
                                      </select>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h4><a href="#">+ Tambah Data</a></h4>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                    <form>
                      <div class="form-group">
                        <label class="control-label">Memo :</label>
                          <textarea class="form-control" rows="4"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-offset-3 col-md-5">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="col-md-3"><h3>Total :</h3></label>
                          <div class="col-md-2">
                            <h3>Rp 0,00</h3>
                          </div>
                      </div>
                    </form>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h4><span class="glyphicon glyphicon-paperclip"></span> Lampiran</h4>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h5><a class="kolom" href="#"> + Tambah Berkas</a></h5>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-offset-7 col-md-5">
                    <div class="col-md-1">
                      <a class="btn btn-danger" href="<?= base_url('admin/biaya') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                      <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Biaya Baru</button>
                    </div>
                  </div>
              </div>
            </header>
          </div>
        </section>
        <div id="bayaran" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Akun Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-3">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Nomor :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Kategori :</label>
                      <div class="col-md-3">
                        <select name="" class="selectpicker" data-live-search="true">
                          <option>Tunai Dan Bank</option>
                        </select>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                    <a class="btn btn-danger" href="<?= base_url('admin/buatbiaya') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="terima" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Wujud Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama Panggilan :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Email :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Alamat Penagihan :</label>
                      <div class="col-md-7">
                        <textarea class="form-control" rows="3"></textarea>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatbiaya') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="carabayar" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Metode Pembayaran Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatbiaya') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>