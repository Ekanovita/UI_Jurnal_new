<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
              <h1><font color="#093C7D">Eka Novita Christin</font></h1>
              <div class="row">
                <div class="col-md-12">
                  <a class="btn btn-info" href="<?= base_url('admin/profil') ?>">PROFIL AKUN</a>
                  <a class="btn btn-info" href="<?= base_url('admin/perusahaan') ?>">DAFTAR PERUSAHAAN</a>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
              <div class="box3">
                <div class="box-header3 with-border3">
                  <h4 class="box-title">Rincian</h4>
                </div>
                <div class="box-body">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <label class="col-md-5">Nama</label>
                        <div class="col-md-5">
                          <h4>Eka Novita Christin</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-5">Ganti Nama</label>
                        <div class="col-md-5">
                          <input type="text" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-5">Email</label>
                        <div class="col-md-5">
                          <h4>ekavita.30@gmail.com</h4>
                        </div>
                    </div>
                  </form>
                </div> 
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
              <div class="box3">
                <div class="box-header3 with-border3">
                  <h4 class="box-title">Kata Sandi</h4>
                </div>
                <div class="box-body">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <label class="col-md-5">Kata Sandi</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-5">Kata Sandi Baru</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-5">Konfirmasi Kata Sandi Baru</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control">
                        </div>
                    </div>
                  </form>
                </div> 
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-9 col-md-3">
              <a class="btn btn-success" href="#"><span class="glyphicon glyphicon-ok"></span> Simpan</a>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>