<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Transaksi</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Buat Penagihan Pembelian</font></h2>
                </div>
              </div>
              <hr><br>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <form class="form-horizontal">
                      <div class="row">
                        <div class="form-group">
                          <label class="col-md-1">Supplier</label>
                            <div class="col-md-3">
                              <select name="" class="selectpicker">
                                <option selected="selected"></option>
                              </select>
                            </div>
                          <label class="col-md-1">Email</label>
                            <div class="col-md-3">
                              <input type="text" class="form-control">
                            </div>
                            <div class="col-md-3">
                              <h3>Total Kuitansi<font color="blue"> (Rupiah)</font></h3>
                            </div>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-offset-2 col-md-10">
                  <h5><a class="kolom" href="#pelanggan" data-toggle="modal"> + Tambah Pelanggan</a></h5>
                </div>
              </div>
              <hr><br>
              <div class="row">
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Memo</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                  </form>
                </div>
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">Tgl Transaksi</label>
                        <div class='input-group date' id='datepicker'>
                          <input type='text' class="form-control" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Tgl Jatuh Tempo</label>
                        <div class='input-group date' id='datepicker'>
                          <input type='text' class="form-control" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Syarat Pembayaran</label>
                        <select name="" class="form-control">
                          <option value="">custom</option>
                          <option value="">Bayar Ditempat</option>
                          <option value="">Net 15</option>
                          <option value="">Net 60</option>
                        </select>
                        <h5><a class="kolom" href="#pembayaran" data-toggle="modal"> + Tambahkan Syarat</a></h5>
                    </div>
                  </form>
                </div>
                <div class="col-md-4">
                  <form>
                    <div class="form-group">
                      <label class="control-label">No. Transaksi</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="control-label">No. Referensi Supplier</label>
                        <input type="text" class="form-control">
                    </div>
                  </form>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="th">Product</th>
                          <th class="th">Deskripsi</th>
                          <th class="th">Kuantitas</th>
                          <th class="th">Satuan</th>
                          <th class="th">Harga Satuan</th>
                          <th class="th">Jumlah</th>
                          <th class="th">+Pajak</th>
                          <th class="th">Hapus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-3">
                                        <h5><a class="kolom" href="#bayar" data-toggle="modal">+Tambah</a></h5>
                                      </div>
                                      <div class="col-md-4">
                                        <select name="" class="selectpicker" data-live-search="true">
                                          <option value="1">Penjualan</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-5">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control" disabled="disabled">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-1">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="checkbox">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-3">
                                        <h5><a class="kolom" href="#bayar" data-toggle="modal">+Tambah</a></h5>
                                      </div>
                                      <div class="col-md-6">
                                        <select name="" class="selectpicker" data-live-search="true">
                                          <option value="1">Penjualan</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-7">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-5">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control" disabled="disabled">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class ="td">
                            <div class="col-md-1">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="checkbox">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="row">
              <div class="col-md-12">
                <h5><a class="kolom" href="#"> + Tambah Data Baru</a></h5>
              </div>
              <div class="col-md-4">
                <form>
                  <div class="form-group">
                    <label class="control-label">Pesan</label>
                      <textarea class="form-control" rows="4"></textarea>
                    <label class="control-label">Memo</label>
                      <textarea class="form-control" rows="4"></textarea>
                  </div>
                </form>
              </div>
              <div class="col-md-offset-3 col-md-5">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-7">Sub Total</label>
                      <div class="col-md-1">
                        <h5>Rp 0,00</h5>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2">Pajak</label>
                      <div class="col-md-5">
                        <select type="text" class="form-control">
                      </select>
                      </div>
                      <div class="col-md-1">
                        <h5>Rp 0,00</h5>
                      </div>
                  </div> 
                  <div class="form-group">
                    <label class="col-md-7">Total</label>
                      <div class="col-md-1">
                        <h5>Rp 0,00</h5>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-7"><h2>Sisa Tagihan</h2></label>
                      <div class="col-md-1">
                        <h5>Rp 0,00</h5>
                      </div>
                  </div>
                </form>
              </div>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h4><span class="glyphicon glyphicon-paperclip"></span> Lampiran</h4>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h5><a class="btn btn-info" href="#"> + Tambah Berkas</a></h5>
                  </div>
                </div>
              <div class="row">
                  <div class="col-md-offset-7 col-md-5">
                    <div class="col-md-1">
                      <a class="btn btn-danger" href="<?= base_url('admin/pembelian') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                      <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Penjualan</button>
                    </div>
                  </div>
              </div>
            </header>
          </div>
        </section>
        <div id="pembayaran" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Syarat Pembayaran Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Janga Waktu :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatpembelian') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="pelanggan" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Wujud Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama Panggilan :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Email :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Alamat Penagihan :</label>
                      <div class="col-md-7">
                        <textarea class="form-control" rows="3"></textarea>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatpembelian') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="bayar" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Product Baru</font></h2>
              </div>
              <div class="modal-body">
                  <div class="col-md-6">
                    <form>
                      <div class="form-group">
                        <label class="control-label">Nama :</label>
                            <input type="text" class="form-control">
                        <label class="control-label">Kode Product :</label>
                            <input type="text" class="form-control">
                      </div>
                    </form>
                  </div>
                  <div class="col-md-6">
                    <form>
                      <div class="form-group">
                        <label class="control-label">Satuan :</label>
                            <select type="text" class="selectpicker">
                              <option value="">Buah</option>
                            </select>
                        <label class="control-label"></label>
                        <h5><a class="kolom" href="#satuan" data-toggle="modal">+Tambah</a></h5>
                      </div>
                    </form>
                  </div>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/buatpembelian') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>