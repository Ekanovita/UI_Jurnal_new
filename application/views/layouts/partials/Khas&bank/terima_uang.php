<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Transaksi</font></h4>
              <div class="row">
                <div class="col-md-12">
                  <h2><font color="#093C7D">Terima Uang</font></h2>
                </div>
              </div>
              <hr>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <form class="form-horizontal">
                      <div class="row">
                        <div class="form-group">
                          <label class="col-md-1">Setor Ke :</label>
                            <div class="col-md-3">
                              <select name="" class="selectpicker" data-live-search="true">
                                <option>pilih</option>
                                  <option>(1-1000)- Kas (Kas & Bank)</option>
                                  <option>(1-1001)- Rekening Bank</option>
                                </select>
                            </div>
                            <div class="col-md-1">
                              <h5><a class="btn btn-info" href="#modal" data-toggle="modal"> + Tambah baru</a></h5>
                            </div>
                            <div class=" col-md-offset-1 col-md-4">
                              <h1>Total Kuitansi<font color="blue"> (Rupiah)</font></h1>
                            </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                      <form class="form-horizontal">
                        <div class="row">
                          <div class="form-group">
                            <label class="col-md-1">Yang Membayar</label>
                              <div class="col-md-3">
                                <select name="" class="selectpicker">
                                  <option value="1">None</option>
                                </select>
                                <h5><a class="kolom" href="#membayar" data-toggle="modal"> + Tambah</a></h5>
                              </div>
                            <label class="col-md-1">Tanggal Transaksi:</label>
                              <div class="col-md-2">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right datepicker" id="datepicker">
                                </div>
                              </div>
                            <label class="col-md-1">No. Transaksi :</label>
                              <div class="col-md-2">
                                <input type='text' class="form-control">
                              </div>
                          </div>
                        </div>
                      </form>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="th">Terima Dari</th>
                          <th class="th">Deskripsi</th>
                          <th class="th">Jumlah</th>
                          <th class="th">Hapus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-2">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                      <select name="" class="selectpicker" data-live-search="true">
                                        <option value="1">(1-1000)- Kas (Kas & Bank)</option>
                                        <option value="2">(1-1001)- Rekening Bank</option>
                                        <option>(1-1200)- Piutang Usaha (Akun Piutang)</option>
                                      </select>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-2">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                      <select name="" class="selectpicker" data-live-search="true">
                                        <option value="1">(1-1000)- Kas (Kas & Bank)</option>
                                        <option value="2">(1-1001)- Rekening Bank</option>
                                        <option>(1-1200)- Piutang Usaha (Akun Piutang)</option>
                                      </select>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h4><a class="kolom" href="#"> + Tambah Data</a></h4>
                  </div>
                </div>
                <br><br>
                <div class="row">
                  <div class="col-md-6">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class=" col-md-offset-1 col-md-2">Memo :</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="4"></textarea>
                          </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-6">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="col-md-3"><h3>Total :</h3></label>
                          <div class="col-sm-8">
                            <h3>Rp 0,00</h3>
                          </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h4><span class="glyphicon glyphicon-paperclip"></span> Lampiran</h4>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11">
                    <h5><a class="btn btn-info" href="#"> + Tambah Berkas</a></h5>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-7 col-md-5">
                    <div class="col-md-1">
                      <a class="btn btn-danger" href="<?= base_url('admin/khas') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                      <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Penerimaan</button>
                    </div>
                  </div>
                </div>
            </header>
          </div>
        </section>
        <div id="modal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Kas Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-3">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Nomor :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Kategori :</label>
                      <div class="col-md-3">
                        <select name="" class="selectpicker" data-live-search="true">
                          <option>Tunai Dan Bank</option>
                        </select>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                    <a class="btn btn-danger" href="<?= base_url('admin/terima') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="membayar" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Wujud Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama Panggilan :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Tipe :</label>
                      <div class="col-md-7">
                        <select name="" class="form-control">
                          <option value="1">Pelanggan</option>
                          <option value="2">Vendor</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Email :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Alamat Penagihan :</label>
                      <div class="col-md-7">
                        <textarea class="form-control" rows="3"></textarea>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-2 col-md-2">
                  <a class="btn btn-danger" href="<?= base_url('admin/terima') ?>">Batal</a>
                </div>
                <div class="col-md-offset-2 col-sm-3">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>