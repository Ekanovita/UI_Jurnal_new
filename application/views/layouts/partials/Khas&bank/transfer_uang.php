<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Transaksi</font></h4>
              <div class="row">
                <div class="col-md-8">
                  <h2><font color="#093C7D">Transfer Uang</font></h2>
                </div>
              </div>
               <hr><br>
              	<div class="row">
	                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
	                	<form class="form-horizontal">
	                		<div class="row">
	                			<div class="form-group">
	                				<label class="col-md-1">Transfer Dari</label>
	                					<div class="col-md-3">
			                				<select name="" class="selectpicker" data-live-search="true">
			                				  <option>Pilih</option>
											  <option>(1-1000)- Kas (Kas & Bank)</option>
											  <option>(1-1001)- Rekening Bank</option>
											</select>
										</div>
	                				<label class="col-md-1">Setor Ke</label>
	                					<div class="col-md-3">
			                				<select name="" class="selectpicker" data-live-search="true">
			                				  <option>pilih</option>
											  <option>(1-1000)- Kas (Kas & Bank)</option>
											  <option>(1-1001)- Rekening Bank</option>
											</select>
										</div>
	                				<label class="col-md-1">Jumlah</label>
	                					<div class="col-md-3">
		                					<input type="text" class="form-control">
		                				</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-1 col-md-11">
						<h5><a class="btn btn-info" href="#modal" data-toggle="modal"> + Tambah Item</a></h5>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-md-2">Memo :</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="4"></textarea>
									</div>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-md-3">No. Transaksi :</label>
									<div class="col-sm-8">
										<input type="text" class="form-control">
									</div>
							</div>
							<div class="form-group">
								<label class="col-md-3">Tgl Transaksi :</label>
									<div class="col-sm-8">
										<div class="input-group">
                                  			<div class="input-group-addon">
                                    			<i class="fa fa-calendar"></i>
                                  			</div>
                                  			<input type="text" class="form-control pull-right datepicker" id="datepicker">
                                  		</div>
                                	</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-1 col-md-11">
						<h4><span class="glyphicon glyphicon-paperclip"></span> Lampiran</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-1 col-md-11">
						<h5><a class="btn btn-info" href="#"> + Tambah Berkas</a></h5>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-7 col-md-5">
						<div class="col-md-1">
							<a class="btn btn-danger" href="<?= base_url('admin/khas') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
						</div>
						<div class="col-md-offset-2 col-md-2">
							<button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Tansfer Uang</button>
						</div>
					</div>
				</div>
	        </header>
          </div>
        </section>
        <div id="modal" class="modal fade">
	        <div class="modal-dialog">
	        	<div class="modal-content">
	        		<div class="modal-header">
	        			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    	<h2><font color="#093C7D">Kas Baru</font></h2>
	                </div>
	                <div class="modal-body">
		                <form class="form-horizontal">
	                        <div class="form-group">
	                            <label class="col-md-3">Nama :</label>
	                            <div class="col-md-7">
	                              	<input type="text" class="form-control">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                         	<label class="col-md-3">Nomor :</label>
	                              	<div class="col-md-7">
	                                  	<input type="text" class="form-control">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                           	<label class="col-md-3">Kategori :</label>
	                             	<div class="col-md-3">
	                                  	<select name="" class="selectpicker" data-live-search="true">
					                        <option>Tunai Dan Bank</option>
	                                  	</select>
	                                </div>
	                        </div>
	                	</form>
	                </div>
	                <div class="modal-footer">
	                	<div class="col-md-offset-2 col-md-2">
		                    <a class="btn btn-danger" href="<?= base_url('admin/transfer') ?>">Batal</a>
		                </div>
		                <div class="col-md-offset-2 col-sm-3">
			                <button class="btn btn-success" name="submit">Simpan</button>
						</div>
	                </div>
	            </div>
	        </div>
    	</div>

<?php endsection() ?>
<?php getview('layouts/home') ?>