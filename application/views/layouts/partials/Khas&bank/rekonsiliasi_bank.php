<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Akun</font></h4>
              <div class="col=md-12">
                <h2><font color="#093C7D">Rekonsiliasi Bank</font></h2>
              </div>
              <hr><br>
              <div class="row">
              	<div class="col-md-offset-1 col-md-11">
              	  <form class="form-horizontal">
	              	  <div class="form-group">
	              	  	<label class="col-md-3">Akun Direkonsiliasi :</label>
	              	  		<div class="col-md-6">
	              	  			<select name="" class="form-control">
	              	  				<option value="">(1-1000)- Kas (Kas & Bank)</option>
	              	  				<option value="">(1-1001)- Rekening Bank (Kas & Bank)</option>
	              	  			</select>
	              	  		</div>
	              	  </div>
	              	  <div class="form-group">
	              	  	<label class="col-md-3">Tanggal Rekonsiliasi Terakhir :</label>
	              	  		<div class="col-md-6">
	              	  			<h3><b>-</b></h3>
	              	  		</div>
	              	  </div>
	              	  <div class="form-group">
	              	  	<label class="col-md-3">Tanggal Rekonsiliasi :</label>
	              	  		<div class="col-md-6">
	              	  			<div class='input-group date' id='datepicker'>
                                  	<input type='text' class="form-control" />
                                    	<span class="input-group-addon">
                                      		<span class="glyphicon glyphicon-calendar"></span>
                                    	</span>
                                </div>
                            </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Saldo Akhir Rekening Koran :</label>
                        	<div class="col-md-6">
                        		<input type="text" class="form-control">
                        	</div>
	              	  </div>
	              </form>
              	</div>
              </div>
              <div class="row">
                <div class="col-md-offset-7 col-md-5">
                	<div class="col-md-1">
                      <a class="btn btn-danger" href="<?= base_url('admin/khas') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                      <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Lanjut</button>
                    </div>
                </div>
              </div>
            </header>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>