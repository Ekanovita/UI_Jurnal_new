<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Kas & Bank</font></h4>
              <div class="row">
                <div class="col-md-10">
                  <h2><font color="#093C7D">Akun Kas</font></h2>
                </div>
                <div class="col-md-2">
                  <a class="btn btn-info" href="#mymodel" data-toggle="modal"><i class='fa fa-plus'></i> <span> Buat Akun Baru</span></a>
                </div>
              </div>
                  	<div id="mymodel" class="modal fade">
	        				<div class="modal-dialog">
	           					<div class="modal-content">
	                				<div class="modal-header">
	                    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    				<h2><font color="#093C7D">Buat Akun Baru</font></h2>
	                				</div>
	                				<div class="modal-body">
		                    			 <form class="form-horizontal">
	                            			<div class="form-group">
	                              				<label class="col-md-3">Nama :</label>
	                              				<div class="col-md-7">
	                                  				<input type="text" class="form-control">
	                                  			</div>
	                                		</div>
	                                		<div class="form-group">
	                              				<label class="col-md-3">Nomor :</label>
	                              				<div class="col-md-7">
	                                  				<input type="text" class="form-control">
	                                  			</div>
	                                		</div>
	                                		<div class="form-group">
	                              				<label class="col-md-3">Deskripsi :</label>
	                              				<div class="col-md-7">
	                                  				<textarea class="form-control" rows="3"></textarea>
	                                  			</div>
	                                		</div>
	                                		<div class="form-group">
	                              				<label class="col-md-3">Kategori :</label>
	                                			<div class="col-md-7">
	                                  				<select name="" class="form-control">
					                                    <option value="1">Tunai Dan Bank</option>
					                                    <option value="2">Kartu Kredit</option>
	                                  				</select>
	                                			</div>
	                            			</div>
	                            			<div class="form-group">
	                              				<label class="col-md-3">Detail :</label>
	                                			<div class="col-md-7">
	                                  				<select name="" class="form-control">
					                                    <option value="1">None</option>
					                                    <option value="2">Sub-Akun-Dari :</option>
					                                    <option value="2">Akun-Header-Dari :</option>
	                                  				</select>
	                                			</div>
	                            			</div>
	                            			<div class="form-group">
	                              				<label class="col-md-3">Saldo Awal :</label>
	                              				<div class="col-md-7">
	                                  				<input type="text" class="form-control">
	                                  			</div>
	                                		</div>
	                                		<div class="form-group">
	                                			<label class="col-md-3">Tanggal mulai:</label>
	                              				<div class="col-md-7">
	                                				<div class='input-group date' id='datepicker'>
	                                  					<input type='text' class="form-control" />
	                                    				<span class="input-group-addon">
	                                      				<span class="glyphicon glyphicon-calendar"></span>
	                                    				</span>
	                                				</div>
	                              				</div>
	                              			</div>
	                              		</form>
	                				</div>
	                				<div class="modal-footer">
	                					<div class="col-md-offset-2 col-md-2">
		                    				<a class="btn btn-danger" href="<?= base_url('admin/khas') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
		                    			</div>
		                    			<div class="col-md-offset-2 col-sm-3">
			                    			<select class="selectpicker" data-style="btn btn-success" data-width="fit">
											  <option>Buat Akun</option>
											  <option>Buat & Baru</option>
											</select>
										</div>
	                				</div>
	            				</div>
	        				</div>
    				</div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <div class="box2">
                <div class="box-header2 with-border2">
                  <h5 class="box-title">Pemasukan 30-hari Mendatang (dalam IDR)<small class="label pull-right bg-green">0</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
            <div class="col-md-3">
              <div class="box1">
                <div class="box-header1 with-border1">
                  <h5 class="box-title">Pengeluaran 30-hari Mendatang (dalam IDR)<small class="label pull-right bg-red">0</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
            <div class="col-md-3">
              <div class="box3">
                <div class="box-header3 with-border3">
                  <h5 class="box-title">Saldo Kas (dalam IDR)<small class="label pull-right bg-blue">2</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
            <div class="col-md-3">
              <div class="box3">
                <div class="box-header3 with-border3">
                  <h5 class="box-title">Saldo Kartu Kredit (dalam IDR)<small class="label pull-right bg-blue">0</small></h5>
                </div>
                <div class="box-body">
                  <h3>Rp 0,00</h3>
                </div><!-- /.box-body --><!-- /.box-footer-->
              </div></a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box4">
                <div class="box-header4 with-border4">
                  <h4 class="col-md-6">DAFTAR AKUN KAS</h4>
                    <div class="col-md-6">
                      <a class="btn btn-info" href="<?= base_url('admin/transfer') ?>">Transfer Uang</a>
                      <a class="btn btn-info" href="<?= base_url('admin/terima') ?>">Terima Uang</a>
                      <a class="btn btn-info" href="<?= base_url('admin/kirimuang') ?>">Kirim uang</a>
                      <a class="btn btn-info" href="<?= base_url('admin/rekonsiliasibank') ?>">Rekonsiliasi Bank</a>
                    </div>
                </div>
                <div class="box-body">
                  <table class="table">
                    <tr class="tr">
                      <th class="th">Kode Akun</th>
                      <th class="th">Nama Akun</th>
                      <th class="th">Saldo (dalam IDR)</th>
                    </tr>
                    <tr>
                      <td class="td">Kas Dan Bank</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td class="td">1-1000</td>
                      <td><a class="kolom" href="#">Kas</td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td">1-1001</td>
                      <td>Rekening Bank</td>
                      <td class="td">0,00</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>