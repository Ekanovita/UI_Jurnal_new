<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Akun</font></h4>
              <div class="row">
                <div class="col-md-8">
                  <h2><font color="#093C7D">Penutupan Buku</font></h2>
                </div>
                <div class="col-md-4">
                  <a class="btn btn-info" href="#"><h4>+ Mulai Tutup Buku</h4></a>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box4">
                <div class="box-header4 with-border4">
                </div>
                <div class="box-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="th">Periode</th>
                        <th class="th">Catatan</th>
                        <th class="th">Keuntungan Bersih (Rugi)</th>
                        <th class="th">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="td">Dari Awal- Tgl Akhir</a></td>
                        <td class="td"></td>
                        <td class="td"></td>
                        <td class="td">
                            <form class="form-inline">
                              <div class="form-group">
                                <label></label>
                                  <div class="input-group">
                                    <select name="" class="selectpicker">
                                      <option value="">Ubah Periode</option>
                                      <option value="">Cek Rekonsiliasi</option>
                                      <option value="">Kertas Kerja</option>
                                      <option value="">Hapus Draf</option>
                                    </select>
                                      <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Go!</button>
                                      </span>
                                  </div>
                              </div>
                            </form>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>