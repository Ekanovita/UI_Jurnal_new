<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Transaksi</font></h4>
              <div class="row">
                <div class="col-md-8">
                  <h2><font color="#093C7D">Jurnal Umum</font></h2>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <form>
                    <div class="form-group">
                      <div class="col-md-3">
                        <label class="control-label">No. Transaksi</label>
                          <input type="text" class="form-control">
                      </div>
                      <div class="col-md-3">
                        <label class="control-label">Tgl Transaksi</label>
                          <div class='input-group date' id='datepicker'>
                            <input type='text' class="form-control" />
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <table class="table">
                      <thead>
                        <tr>
                          <th class="th">Akun</th>
                          <th class="th">Deskripsi</th>
                          <th class="th">Debit</th>
                          <th class="th">Kredit</th>
                          <th class="th"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="td">
                            <div class="col-sm-offset-1 col-md-3">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                      <select name="" class="selectpicker" data-live-search="true">
                                        <option value="1">(1-1000)- Kas (Kas & Bank)</option>
                                        <option value="2">(1-1001)- Rekening Bank</option>
                                        <option>(1-1200)- Piutang Usaha (Akun Piutang)</option>
                                      </select>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <textarea class="form-control" rows="1"></textarea>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td">
                            <div class="col-md-8">
                              <form class="form-horizontal">
                                <div class="row">
                                  <div class="form-group">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </td>
                          <td class="td"><small class="label pull-right bg-red"><span class="glyphicon glyphicon-remove"></span></small></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-offset-1 col-md-11">
                  <h5><a class="kolom" href="#">+ Tambah Data</a></h5>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <form>
                    <div class="form-group">
                      <div class="col-md-4">
                        <label class="control-label">Memo</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                      <div class="col-md-offset-4 col-md-2">
                        <label class="control-label">Total Debit</label>
                          <h4>Rp 0,00</h4>
                      </div>
                      <div class="col-md-2">
                        <label class="control-label">Total Kredit</label>
                          <h4>Rp 0,00</h4>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="row">
                <div class="col-md-offset-1 col-md-11">
                  <form>
                    <div class="form-group">
                      <label class="control-label"><span class="glyphicon glyphicon-paperclip"></span> Lampiran</label>
                        <h5><a class="kolom" href="#">+ Tambah Berkas</a></h5>
                    </div>
                  </form>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section>
          <div class="row">
            <div class="col-md-offset-8 col-md-2">
              <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Buat Jurnal Umum</button>
            </div>
            <div class="col-md-1">
              <a class="btn btn-danger" href="<?= base_url('admin/akun') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
            </div>
          </div>
          <br>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>