<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Akun</font></h4>
              <div class="row">
                <div class="col-md-8">
                  <h2><font color="#093C7D">Daftar Akun</font></h2>
                </div>
                <div class="col-md-4">
                  <a class="btn btn-info" href="<?= base_url('admin/jurnalumum') ?>"><i class='fa fa-plus'></i> <span> Buat Jurnal Umum</span></a>
                  <a class="btn btn-info" href="#akunbaru" data-toggle="modal"><i class='fa fa-plus'></i> <span> Buat Akun Baru</span></a>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box4">
                <div class="box-header4 with-border4">
                  <div class="col-md-7"></div>
                  <div class="col-md-5">
                    <a class="btn btn-default" href="<?= base_url('admin/tutupbuku') ?>"><i class="fa fa-book" aria-hidden="true"></i> Penutupan Buku</a>
                    <a class="btn btn-default" href="#"><i class="fa fa-download" aria-hidden="true"></i> Import</a>
                    <a class="btn btn-default" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Eksport</a>
                  </div>
                </div>
                <div class="box-body">
                  <table class="table">
                    <tr>
                      <th class="th"><input type="checkbox"></th>
                      <th class="th">Kunci</th>
                      <th class="th">Kode Akun</th>
                      <th class="th">Nama Akun</th>
                      <th class="th">Kategori Akun</th>
                      <th class="th">Saldo (dalam IDR)</th>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1000</td>
                      <td><a class="kolom" href="#">Kas</a></td>
                      <td><a class="kolom" href="#">Cash & Bank</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1001</td>
                      <td><a class="kolom" href="#">Rekening Bank</a></td>
                      <td><a class="kolom" href="#">Cash & Bank</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                      <td class="td"><span class="glyphicon glyphicon-lock"></span> <i class="fa fa-plus" aria-hidden="true"></i></td>
                      <td class="td">1-1200</td>
                      <td><a class="kolom" href="#">Piutang Usaha</a></td>
                      <td><a class="kolom" href="#">Accounts Receivable (A/R)</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1210</td>
                      <td><a class="kolom" href="#">Piutang Lainnya</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1300</td>
                      <td><a class="kolom" href="#">Dana Belum Disetor</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                      <td class="td"><span class="glyphicon glyphicon-lock"></span> <i class="fa fa-plus" aria-hidden="true"></i></td>
                      <td class="td">1-1400</td>
                      <td><a class="kolom" href="#">Persediaan Barang</a></td>
                      <td><a class="kolom" href="#">Inventory</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                      <td class="td"><span class="glyphicon glyphicon-lock"></span> <i class="fa fa-plus" aria-hidden="true"></i></td>
                      <td class="td">1-1500</td>
                      <td><a class="kolom" href="#">Uang Muka Pembelian</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1600</td>
                      <td><a class="kolom" href="#">Pinjaman Karyawan</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1700</td>
                      <td><a class="kolom" href="#">Pinjaman Lainnya</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                      <td class="td"><span class="glyphicon glyphicon-lock"></span> <i class="fa fa-plus" aria-hidden="true"></i></td>
                      <td class="td">1-1800</td>
                      <td><a class="kolom" href="#">Aset Tetap</a></td>
                      <td><a class="kolom" href="#">Fixed Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1801</td>
                      <td><a class="kolom" href="#">Penyusutan Aset Tetap</a></td>
                      <td><a class="kolom" href="#">Depreciation & Amortization</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1810</td>
                      <td><a class="kolom" href="#">Aset Tak Berwujud</a></td>
                      <td><a class="kolom" href="#">Fixed Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                    <tr>
                      <td class="td"><input type="checkbox"></td>
                      <td class="td">-</td>
                      <td class="td">1-1820</td>
                      <td><a class="kolom" href="#">Investasi</a></td>
                      <td><a class="kolom" href="#">Other Current Assets</a></td>
                      <td class="td">0,00</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div id="akunbaru" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2><font color="#093C7D">Buat Akun Baru</font></h2>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-md-4">Nama :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Nomor :</label>
                      <div class="col-md-7">
                        <input type="text" class="form-control">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Deskripsi :</label>
                      <div class="col-md-7">
                        <textarea class="form-control" rows="3"></textarea>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Kategori :</label>
                      <div class="col-md-7">
                        <select name="" class="selectpicker" data-live-search="true">
                          <option value="1">Akun Piutang</option>
                          <option value="2">Aktiva Lancar Lainnya</option>
                          <option value="2">Kas & Bank</option>
                          <option value="2">Persediaan</option>
                          <option value="2">Aktiva Tetap</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Details :</label>
                      <div class="col-md-7">
                        <select name="" class="selectpicker" data-live-search="true">
                          <option value="1">Kosong</option>
                          <option value="2">Sub Akun Dari:</option>
                          <option value="2">Akun Header Dari:</option>
                        </select>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <div class="col-md-offset-8 col-md-2">
                  <button class="btn btn-success" name="submit">Simpan</button>
                </div>
                <div class="col-md-1">
                  <a class="btn btn-danger" href="<?= base_url('admin/akun') ?>">Batal</a>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php endsection() ?>
<?php getview('layouts/home') ?>