<?php section('content') ?>
          <div id='main-content'>
            <header class='page-heading'>
                <h4><font color="gray">Pelanggan</font></h4>
              <div class="row">
                <div class="col-md-9">
                  <h2><font color="#093C7D">Buat Pelanggan Baru</font></h2>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-6">
                    <form>
                      <div class="form-group">
                        <label class="control-label">Nama Perusahaan</label>
                          <input type="text" class="form-control">
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-3">
                            <label class="control-label">Gelar</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-3">
                            <label class="control-label">Nm Depan</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-3">
                            <label class="control-label">Nm Tengah</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-3">
                            <label class="control-label">Nm Keluarga</label>
                              <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Nama Panggilan</label>
                          <input type="text" class="form-control">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Alamat Penagihan</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Alamat Pengiriman</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-6">
                    <form>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Email</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">NPWP</label>
                              <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-4">
                            <label class="control-label">Mobile</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="control-label">Telepon</label>
                              <input type="text" class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="control-label">Fax</label>
                              <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label">informasi Lainnya</label>
                            <textarea class="form-control" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Akun Piutang</label>
                              <select class="selectpicker" data-live-search="true">
                                <option value="">Piutang Usaha</option>
                              </select>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Akun Hutang</label>
                              <select class="selectpicker" data-live-search="true">
                                <option value="">Hutang Usaha</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Saldo Awal</label>
                            <input class="form-control" type="text">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Tanggal Mulai</label>
                          <div class='input-group date' id='datepicker'>
                            <input type='text' class="form-control" />
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Piutang Maksimal</label>
                              <input class="form-control" type="text">
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Syarat Pembayaran Utama</label>
                              <select class="selectpicker" data-live-search="true">
                                <option value="">Bayar ditempat</option>
                                <option value="">Net 15</option>
                                <option value="" selected="selected">Net 30</option>
                                <option value="">Net 60</option>
                              </select>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="row">
                    <div class="col-md-offset-6 col-md-6">
                      <input type="checkbox"> 
                        <span>non-aktifkan piutang maksimal</span>
                    </div>
                  </div>
                  <br><br>
                  <div class="row">
                    <div class="col-md-offset-7 col-md-5">
                      <div class="col-md-1">
                        <a class="btn btn-danger" href="<?= base_url('admin/pelanggan') ?>"><span class="glyphicon glyphicon-remove-circle"></span> Batal</a>
                      </div>
                      <div class="col-md-offset-2 col-md-2">
                        <button class="btn btn-success" name="submit"><span class="glyphicon glyphicon-ok-circle"></span> Buat Pelanggan Baru</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
<?php endsection() ?>
<?php getview('layouts/home') ?>