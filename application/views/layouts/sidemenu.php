          
          <ul class="sidebar-menu">
            <li>
              <a href="<?= base_url('') ?>">
                <i class="fa fa-dashboard"></i><span> Dashboard</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/laporan') ?>">
                <i class="fa fa-area-chart"></i><span> Laporan</span> 
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/khas') ?>">
                <i class="fa fa-university"></i> <span>Kas & Bank</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/penjualan') ?>">
                <i class="fa fa-money"></i><span>Penjualan</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/pembelian') ?>">
                <i class="fa fa-credit-card"></i>
                <span>Pembelian</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/biaya') ?>">
                <i class="fa fa-usd"></i> <span>Biaya</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/pelanggan') ?>">
                <i class="fa fa-users"></i> <span>Pelanggan</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/supplier') ?>">
                <i class="fa fa-truck"></i> <span>Suplier</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/product') ?>">
                <i class="fa fa-cube"></i> <span>Produk</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/akun') ?>">
                <i class="fa fa-book"></i> <span>Daftar Akun</span>
              </a>
            </li>
            <li>
              <a href="<?= base_url('admin/daftar') ?>">
                <i class="fa fa-list"></i> <span>Daftar Lainnya</span>
              </a>
            <li>
              <a href="<?= base_url('admin/setting') ?>">
                <i class="fa fa-cogs"></i> <span>Pengaturan</span>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-sign-out"></i><span>Keluar</span>
              </a>
            </li>