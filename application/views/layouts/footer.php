
 <footer class="main-footer">
          <strong>Copyright &copy; 2014-2015 <a href="#">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>

    <!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="<?= base_url('public/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= base_url('public/plugins/bootstrap-select/js/bootstrap-select.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/bootstrap/js/jquery-1.11.1.min.js') ?>"></script>
    <script src="<?= base_url('public/bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- SlimScroll -->
    <script src="<?= base_url('public/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <!-- FastClick -->
    <script src="<?= base_url('public/plugins/fastclick/fastclick.min.js') ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('public/dist/js/app.min.js') ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url('publc/dist/js/demo.js') ?>"></script>
      <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker({
                    format : 'dd-mm-yyyy'
            });
        });
      </script>
    </div>
  </body>
</html>
