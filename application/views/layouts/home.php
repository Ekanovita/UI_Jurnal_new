<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Jurnal Accounting</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/plugins/datepicker/datepicker3.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap-select/css/bootstrap-select.min.css') ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/styles.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/skins/_all-skins.min.css') ?>">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="<?= base_url('') ?>" class="logo fixed">
          <span class="logo-lg"><b>ACCOUNTING</b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a class="btn btn-info" href="<?= base_url('admin/buatpenjualan') ?>">
                  <i class="fa fa-money"></i><span> Jual</span></a></li>
                <li><a class="btn btn-info" href="<?= base_url('admin/buatpembelian') ?>">
                  <i class="fa fa-credit-card"></i><span> Beli</span></a></li>
                <li><a class="btn btn-info" href="<?= base_url('admin/buatbiaya') ?>">
                  <i class="fa fa-usd"></i><span> Biaya</span></a></li>
              </ul>
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Eka Novita (EKA'S GROUP) <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a class="kolom" href="<?= base_url('admin/profil') ?>"><h4>Profil Akun</h4></a></li>
                        <li><a class="kolom" href="<?= base_url('admin/perusahaan') ?>"><h4>Daftar Perusahaan</h4></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a class="kolom" href="<?= base_url('') ?>"><h3>Eka's Group (Aktif)</h3></a></li>
                      </ul>
                  </li>
                </ul>
              </div>
            </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <section class="sidebar">
            <?php $this->load->view('layouts/sidemenu') ?>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->
      <div class="content-wrapper">
        <section>
          <?php render('content') ?>
      </div>
    <?php $this->load->view('layouts/footer') ?>