

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	
	public function index()
	{
		$this->load->view('layouts/partials/dashboard');
	}
	public function laporan()
	{
		$this->load->view('layouts/partials/laporan/home_lpr');
	}
	public function khas()
	{
		$this->load->view('layouts/partials/khas&bank/home_kas');
	}
	public function penjualan()
	{
		$this->load->view('layouts/partials/penjualan/home_penjualan');
	}
	public function pembelian()
	{
		$this->load->view('layouts/partials/pembelian/home_pembelian');
	}
	public function biaya()
	{
		$this->load->view('layouts/partials/biaya/home_biaya');
	}
	public function pelanggan()
	{
		$this->load->view('layouts/partials/pelanggan/home_pelanggan');
	}
	public function supplier()
	{
		$this->load->view('layouts/partials/supplier/home_supplier');
	}
	public function product()
	{
		$this->load->view('layouts/partials/product/home_product');
	}
	public function akun()
	{
		$this->load->view('layouts/partials/akun/home_akun');
	}
	public function daftar()
	{
		$this->load->view('layouts/partials/daftar/home_daftar');
	}
	public function setting()
	{
		$this->load->view('layouts/partials/setting/home_setting');
	}
	public function labarugi()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/laba_rugi');
	}
	public function neraca()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/neraca');
	}
	public function aruskas()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/arus_kas');
	}
	public function bukubesar()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/buku_besar');
	}
	public function rekonsiliasi()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/rekonsiliasi_bank');
	}
	public function jurnal()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/jurnal');
	}
	public function saldo()
	{
		$this->load->view('layouts/partials/laporan/sekilas bisnis/saldo');
	}
	public function faktur()
	{
		$this->load->view('layouts/partials/laporan/penjualan/daftar_faktur');
	}
	public function piutang()
	{
		$this->load->view('layouts/partials/laporan/penjualan/piutang_plgn');
	}
	public function pengiriman()
	{
		$this->load->view('layouts/partials/laporan/penjualan/pengiriman');
	}
	public function penjualanplgn()
	{
		$this->load->view('layouts/partials/laporan/penjualan/penjualan');
	}
	public function usiapiutang()
	{
		$this->load->view('layouts/partials/laporan/penjualan/usia_piutang');
	}
	public function penjualanproduct()
	{
		$this->load->view('layouts/partials/laporan/penjualan/penjualan_product');
	}
	public function daftarbeli()
	{
		$this->load->view('layouts/partials/laporan/pembelian/daftar_beli');
	}
	public function hutangsupplier()
	{
		$this->load->view('layouts/partials/laporan/pembelian/hutang_supplier');
	}
	public function Pengeluaran()
	{
		$this->load->view('layouts/partials/laporan/pembelian/rincian_Pengeluaran');
	}
	public function pengirimanbeli()
	{
		$this->load->view('layouts/partials/laporan/pembelian/laporan_pengiriman');
	}
	public function pembelianper()
	{
		$this->load->view('layouts/partials/laporan/pembelian/pembelian_supplier');
	}
	public function dftrpengeluaran()
	{
		$this->load->view('layouts/partials/laporan/pembelian/daftar_pengeluaran');
	}
	public function piutangbeli()
	{
		$this->load->view('layouts/partials/laporan/pembelian/umur_piutang');
	}
	public function pembelianproduct()
	{
		$this->load->view('layouts/partials/laporan/pembelian/pembelian_product');
	}
	public function transfer()
	{
		$this->load->view('layouts/partials/khas&bank/transfer_uang');
	}
	public function terima()
	{
		$this->load->view('layouts/partials/khas&bank/terima_uang');
	}
	public function kirimuang()
	{
		$this->load->view('layouts/partials/khas&bank/kirim_uang');
	}
	public function rekonsiliasibank()
	{
		$this->load->view('layouts/partials/khas&bank/rekonsiliasi_bank');
	}
	public function buatpenjualan()
	{
		$this->load->view('layouts/partials/penjualan/buat_penjualan');
	}
	public function buatpembelian()
	{
		$this->load->view('layouts/partials/pembelian/buat_pembelian');
	}
	public function buatbiaya()
	{
		$this->load->view('layouts/partials/biaya/buat_biaya');
	}
	public function buatpelanggan()
	{
		$this->load->view('layouts/partials/pelanggan/buat_pelanggan');
	}
	public function buatsupplier()
	{
		$this->load->view('layouts/partials/supplier/buat_supplier');
	}
	public function buatproduct()
	{
		$this->load->view('layouts/partials/product/buat_product');
	}
	public function profil()
	{
		$this->load->view('layouts/partials/profil_akun');
	}
	public function perusahaan()
	{
		$this->load->view('layouts/partials/daftar_perusahaan');
	}
	public function jurnalumum()
	{
		$this->load->view('layouts/partials/akun/daftar_jurnalumum');
	}
	public function tutupbuku()
	{
		$this->load->view('layouts/partials/akun/tutupbuku');
	}
	public function daftartransaksi()
	{
		$this->load->view('layouts/partials/daftar/daftar_transaksi');
	}
}
